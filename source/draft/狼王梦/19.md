---
title: 狼王梦--第2章 狼种 1
tags: 狼王梦
categories:
  - 狼王梦
cover: 'hhttps://img9.doubanio.com/view/ark_article_cover/retina/public/754245.jpg'
date: 2022-01-19 00:00:00
---






　　一被淘汰的警犬
　　它叫大灰，是阳光大马戏团动物特型演员，在舞台上专门扮演大灰狼。
　　在人类演艺圈，要拍政治家、军事家或文化名人的影视剧，挑一个长相酷似的人来出演主角，这就叫特型演员。
　　大灰来阳光大马戏团之前，是黑土凹警犬学校一条即将毕业的警犬。
　　大灰刚生下来时，毛色就让人看着不顺眼。通体紫灰，没有光泽，就像一堆难看的草木灰。
　　警犬学校的狗都是从国外引进的名贵犬种，大致分两种颜色，要么通体金黄，要么是黑与黄组合成的花色，从未有过灰毛狗。
　　它的长相似乎也与正常的狗崽有差异。正常的狗崽嘴吻圆润，耳廓柔软如花瓣，半只耳朵皮盖在额头上，显示出狗顺从的品性。大灰的嘴吻又尖又长，三角形耳廓坚挺耸立，透出一股让人感觉不舒服的倔强神情。
　　不能以貌取人，当然也不能以皮毛的颜色和五官的美丑来取狗。
　　三个月大后，大灰和其他小狗一起接受严格的警犬训练。
　　大灰在训练场上表现出色，但随着身体发育成熟，相貌却越来越离谱。
　　所有人都觉得，大灰不像一条狗，倒像是一只狼。
　　在动物分类学上，狼和狗虽然同为犬科犬属，血缘近亲，体貌相似，但仔细鉴别狼和狗的五官和身体外形，就会发现它们之间还是有明显区别的。
　　从体态上说，狼和狗显著的差异在胸部，狼的胸部较狭窄，狗的胸部较开阔。不用尺子来量，一眼就能看出大灰比其他警犬胸部窄了约一寸多。
　　从五官上说，狼吻部尖长嘴巴特别阔大，这便于在野外噬咬猎物，狼眼比狗眼更细长些，瞳人却比狗的瞳仁明显要小，狼眼是白多黑少，所以民间有白眼狼的说法。用这标准来衡量，大灰活脱脱长着一副狼的尊容。
　　从尾巴上说，狗尾巴紧凑光滑，当主人抚摸它们时，会像蛇一样活泼地曲卷摇动，而大灰的尾巴却蓬松如扫帚，僵硬得像根棍子，或者平举或者耷拉，最多只能左右笨拙地甩动两下，典型的狼尾巴。
　　要说狼和狗最大的区别，在于不同的叫声，狗会吠叫，汪汪汪声音短促嘹亮，富有音乐的节奏感，而狼不会吠只会嗥叫，叫声绵长中间没有间隔，音质尖厉缺乏变化。大灰怎么努力也叫不出清晰的狗吠声，只能发出凄厉难听的狼嚎。
　　警犬学校每一条狗都有完整的档案资料，细细查阅后发现，大灰的祖母是一条法国狼犬，有十六分之一狼的血统，大灰的母亲是一条德国杜宾犬，有六十四分之一狼的血统。
　　一般来讲，只要低于十六分之一狼的血统，狼的基因就是隐性；换句话说，母狗身上只要狼的血统少于十六分之一，狗的基因就呈显性。
　　大灰的母亲身上只有六十四分之一狼的血统，也就是说只有六十四分之一的可能会生小狼崽，概率是极小的。
　　但遗传密码排序极为复杂，在某种因素干扰下，隐性基因有可能突变成显性，这就是返祖现象。
　　毫无疑问，大灰身上出现了罕见的返祖现象。
　　一年零七个月的学习生涯结束了，大灰完成全部警犬训练课目，蹿跳、奔袭、游泳、嗅闻、追踪、潜伏、捕捉、格斗，考核成绩门门都是优秀。
　　云南地处西南边陲，山高林密交通不便，与罂粟产区金三角相隔不远，贩毒活动十分猖獗。警犬嗅觉灵敏，能从一堆新鲜的椰子里准确找出哪几只藏有海洛因，还能从密不透风的热带雨林里把蜷缩在蟒蛇洞内的毒贩子缉拿归案。一条优秀的警犬抵得上三个平庸的警察。
　　训练有素的警犬供不应求，十分紧俏。
　　“哦，它除了长相不太中看外，其他各方面都很棒，跟别的警犬没什么两样，带回去吧，我敢保证，它不会让你们失望的。”警犬驯养员热情地向边境缉毒分队前来购犬的人推荐介绍。
　　“不行，不行。”人们连连摇头，“牵着狼上山巡逻，会惹出一大堆麻烦来的。”
　　同期毕业的三十来条警犬很快被抢购一空，唯独大灰无人问津。
　　有人提议，将灰狗崽子当做生理有缺陷训练不合格的废犬，按正常程序淘汰，送往林场、货栈或仓库去，做一条守家护院的看门狗。但问了好几个地方，人家一看是一只不会摇尾巴的灰毛狼，立刻婉言谢绝了。
　　又有人提议，把大灰当狼送到动物园去，圆通山动物园有野狼馆，物以类聚，应该说是很恰当的。可动物园的人说，这不是真正的狼，而是一只返祖现象的狗，动物园不能接受一条被遗弃的家犬，不管它长得是不是像狼。
　　如何处置这条容貌异化的狗，竟成了警犬学校的一道难题。
　　二阴差阳错进了马戏团
　　阳光大马戏团高导演参加朋友的生日聚会，来到警犬学校，酒席间听说了大灰的事。
　　开始高导演并没有要招个狗演员的想法，马戏团狗演员阵容最为强大，已经有十二条聪明伶俐的卷毛哈巴狗，没必要再新添狗演员。
　　可当他站到大灰面前时，出于职业的敏感，出于排演新节目的考虑，立马就决定把大灰带回马戏团去。
　　马戏团有许多种类动物演员，狮虎豹象熊狗猫马羊，还有海狮海豹海象等等，唯独没有狼。谁也没有规定说，狼不能上台演出，可全世界所有马戏团，找不到一个狼演员。
　　有人说，是因为狼和狗长得相似，能表演的形体动作也大致相同，各马戏团都豢养着一大群狗演员，没必要再耗费人力财力去培训狼演员了。
　　也有人解释说，之所以全世界马戏团都将狼拒之门外，最根本的原因是，人们对狼有一种根深蒂固的厌恶和憎恨。
　　同样是猛兽，可狮子老虎毛色华丽，形象威武，人类对它们抱有敬畏心理，常用雄狮来比喻刚强的男子汉或骁勇善战的军队，用醒狮来比喻新兴的国家或翻身民族；更有一大串成语用来赞美老虎，什么虎踞龙蟠、龙腾虎跃、虎虎生威、将门虎子等等，因此狮子老虎登上马戏团舞台，体现力量的壮美，观众会产生崇拜认同，别有一番审美情趣。
　　狼就不同了，翻遍字典也找不到一句褒奖的话，什么狼心狗肺、狼子野心、狼狈为奸、豺狼当道、色狼肆虐等等，清一色贬义词。在人们心目中，狼是十恶不赦的坏蛋，狼是恶魔的代名词。再说，狼毛色单调，嘴脸丑陋，缺乏美感，形象也不适合登台演出。
　　人类社会有人种歧视的现象，动物世界难免也有物种歧视的现象。
　　可高导演是个具有开拓创新精神的人，他觉得时代在变，人们的思想观念也随之在变，对狼的认识已不像过去那样极端了，现在观众应该是能接受一只狼登台演出的。
　　大灰酷似桀骜不驯的狼，对外就宣称它是狼，估计没有人会怀疑。
　　阳光大马戏团提的口号就是：人无我有，人有我优。全世界马戏团没有一个狼演员，如果他能成功地让一只狼登上舞台，本身就很新奇有趣，无疑具有广告效应。
　　高导演决定招收大灰，还有一个很重要的原因，刚好能配合他的改革方案，排演从内容到形式都叫绝的新节目。
　　这几年，人们对马戏表演的热情早就衰退，说得刻薄一点，马戏已变成一门夕阳艺术，光顾马戏团的人越来越少。
　　他晓得，除了社会环境变化这个外部因素外，节目老化，也是阳光大马戏团一年比一年不景气的很重要的原因。马走舞步、羊拉三轮、虎钻火圈、熊舞飞叉……就这么一二十个节目翻来覆去地演，天天都是老一套，谁还稀罕来看呀！
　　要想振兴阳光大马戏团，把流失的观众重新拉回剧场来，唯一的办法就是根据青少年追求刺激的心理特点，编排出惊险新奇的节目来。
　　他有个大胆的设想，将几种不同的动物串连起来，动物演员相互配戏，编出有故事情节的马戏短剧。
　　迄今为止，舞台上只有杂技剧，还没有几种动物联袂演出的马戏剧。
　　要想出既有思想教育意义，又能充分发挥各种动物演员技巧特长，又有相当艺术吸引力的故事情节，并非易事。他苦思冥想了好几个月，脑子里仍一片混沌。
　　说也奇怪，高导演站在大灰面前，突然就来了灵感。哦，一只凶残狡猾的大灰狼，要去吃美丽可爱的小白羊，鹦鹉、哈巴狗和黑熊齐心协力斗败了大灰狼，这肯定很有意思，在善良的众动物与恶狼斗智斗勇的过程中，可以穿插许多精彩的杂技动作。
　　剧名就叫《智斗大灰狼》，孩子们肯定喜欢看。
　　警官学校正为如何处置大灰发愁呢，高导演不费吹灰之力，也没花一分钱，就把大灰带回了阳光大马戏团。
　　三扮演大灰狼
　　它在警犬学校叫大灰，到了阳光大马戏团后，大家仍叫它大灰，这虽然不是一个很响亮的艺名，但很贴切，符合它的容貌特征。大灰者，又是大灰狼的简称，含有某种隐喻。
　　阳光大马戏团有个很大的狗棚，里头有用棉毡铺垫的睡窝，有色彩鲜艳的食盆，有触碰式自动饮水机，还有彩球、电子兔、塑料肉骨头等玩具，对狗而言，算得上是功能齐全的豪华别墅了。十二只哈巴狗就住在里头。
　　管理员老费没让大灰住进舒适的狗棚，而是在狗棚边靠近阴沟的一块空地用钢筋焊了一只铁笼子。笼子阴暗潮湿，有一股腐烂的霉味，低矮狭窄，大灰在里头站直了，额头就会顶着天花板，转身时尾巴也会蹭着铁杆，活像在蹲监狱。
　　“白眼大灰狼，只配住这种地方。”老费说。
　　狼在任何地方都属于不受欢迎的角色。
　　负责训练大灰的，是个名叫川妮的女演员，从艺已有七八年时间。她是在高导演严厉训斥下这才皱着眉头含着泪接受训练大灰的任务的。她对团里要好的姐妹说：“看见它我会想起小时候读过的童话里的狼外婆，脊梁就冷飕飕的，心里发毛，头皮也会发麻。”
　　不情愿去做的事情，态度自然很恶劣。
　　“跳，快跳！”川妮柳眉高挑杏眼圆睁，手中的棍子指着两米高的铁圈，大声呵斥。大灰腾空起跳，稳稳当当地从直径约半米的铁圈穿了过去。
　　“站起来，狼爪子举高些！走，快走！”川妮手中的驯兽棍直逼大灰的脑门，模样很像是粗暴的老师在指责一看就让她心烦的顽劣学生。
　　其实川妮是个挺有爱心的女人，蛮喜欢小狗小猫的。
　　“别害怕，乖乖，勇敢些，来，跳呀！”她和颜悦色地对哈巴狗们说。
　　十二只哈巴狗全身披挂白色的长毛，蓝眼睛，塌鼻子，大耳朵，四条腿很短，胖嘟嘟迷你身材，奔跑起来就像一团团滚动的雪球，又滑稽又可爱。
　　当一只名叫杰克的哈巴狗勉勉强强地从铁圈穿越过去，她会温柔地抚摸它的颈毛，亲昵地拧它肉感很强的耳垂，夸奖道：“好样的，真聪明！”
　　训练告一段落，便要给受驯的动物喂点东西吃，有奖赏才有干劲，食物引诱是马戏团培训动物演员的最佳方法。
　　十二只哈巴狗轰的一下把川妮团团围住，拼命往她怀里钻，汪嗯，汪嗯，发出撒娇的哼哼声，伸出粉红色细腻的狗舌，争先恐后地来舔吻她的脸。
　　她咯咯笑着，没法拒绝这多余的热情。
　　她抱抱这个，又亲亲那个，将香喷喷的牛肉干塞进哈巴狗的嘴里。
　　每每这个时候，大灰总是站在幽暗的墙角，目不转睛地盯着那群幸福的哈巴狗看，狭长的狼脸上浮起羡慕的神情。
　　川妮也给大灰喂牛肉干。团里有规定，凡参训的动物演员，无大的过错，中间休息时都要喂一点食物，以形成激励机制。
　　大灰是从警犬学校毕业的高才生，难度再大的训练课目也是小菜一碟，钻圈、蹿高、叼碟、直立、拖物……无论什么动作都完成得精确漂亮，根本挑不出毛病来，是最应该得到牛肉干犒赏的。
　　川妮总是蹙着眉尖，抓一把牛肉干走到大灰面前，板着脸嗯一声，当大灰抻长脖子张开尖尖的嘴吻想要叼食时，她一撒手，将牛肉干扔在地上，任由大灰捡食。
　　那模样，像是护士在尽量避免接触烈性传染病患者。
　　这天上午，与往常一样，训练间歇，哈巴狗们围着川妮，摇尾撒欢，献媚邀宠。
　　大灰不停地伸出血红的舌头舔吻自己的鼻子，瞳人中透出艳羡的光，一副神往已久的表情。它沿着墙线，压低身体，悄悄绕到川妮身后。
　　它虽然外貌像狼，本质上仍是狗，而且是狗类中品性最忠诚的警犬。像所有的狗一样，它渴望能得到主人的搂抱和抚摸，渴望能得到主人的宠爱。
　　它似乎知道这位漂亮的女主人并不喜欢它，所以不敢像哈巴狗们那样放肆地钻到川妮怀里去撒欢，只是躲在背后蜻蜓点水般地用舌尖轻轻亲吻她的衣领。
　　川妮与哈巴狗们嬉闹着，没有察觉大灰已绕到自己背后来了。
　　杰克正趴在川妮的肩头享受主人温馨的爱抚，当然看见大灰做贼似的举动。哈巴狗擅长察言观色，它早就看出女主人讨厌这非狼非狗的家伙。它立刻汪汪汪在主人耳畔吠叫报警：亲爱的主人，背后有紧急情况！
　　川妮猛地回头去看，人眼和白多黑少的狼眼四目相对，她白皙细嫩的脖颈刚好撞在大灰尖尖的嘴吻上。她没思想准备，吓一大跳，失声尖叫起来。
　　美丽的女孩子见到一条毛毛虫爬到身上，或者一只老鼠冷不防从面前蹿过，都会惊骇地大叫起来，一副立刻就要晕倒的表情。
　　大灰噼噼啪啪地左右甩动扫帚般笨拙的尾巴，将半条舌头从唇齿间伸出来，眼珠子尽量睁大，并将自己最易受伤害的颈侧暴露在外，用犬科动物特有的形体语言表白自己的心迹：请相信我，我没有恶意。
　　在川妮眼里，那条左右甩动的大尾巴，是扑咬的信号，拧着脖颈，那是行凶的前奏，眼珠子瞪得这么大，那是饿狼馋涎欲滴，这么长半截血红的舌头伸在嘴外，是不是想尝尝人肉的滋味呀，嘴腔内尖利的犬牙闪着寒光，预示着吃人不吐骨头啊。
　　她粉红的小脸恐怖地扭曲了，用手护住自己的脖子，颤抖的声音高喊道：“你想干什么？滚……滚……快滚开！”
　　大灰胆怯地望着她，后退了两步。
　　川妮倏地站起来，抄起那根驯兽棍，戳着大灰的肩胛，用力推搡，咬牙切齿地喝道：“滚，滚到墙角去，离我远点！”
　　哈巴狗们也龇牙咧嘴地咆哮，跃跃欲扑，凶猛地驱赶大灰。哈巴狗历来看主人的脸色行事，爱主人所爱，恨主人所恨。
　　大灰将扫帚似的大尾巴紧紧夹在胯间，脊背高耸，脑袋缩进颈窝，这是犬科动物被打败后的姿态，呦呦发出刺耳的哀嗥，就像负了重伤一样，脸上的表情异常痛苦，被迫往幽暗的墙角退却。
　　那根八十公分长闪闪发亮的金属驯兽棍并没戳疼大灰，哈巴狗们也只是朝它狺狺狂吠没扑上来撕咬。作为狗，被主人嫌弃，遭主人厌恶，受主人憎恨，是最大的悲哀。它的心在受伤，它的心在滴血，这精神创伤，比皮肉受到鞭笞厉害多了。
　　大灰蜷缩在幽暗的墙角，身体像害了一场大病似的瑟瑟发抖，头埋进臂弯，发出如泣如诉的嗥叫。
　　川妮平静下来，大概意识到自己态度过于粗暴，做得有点太过分了，便抓了一把牛肉干，扔在大灰面前，也算是一种缓和吧。
　　大灰虽然肚皮空瘪瘪，却没有去动这些香浓美味的牛肉干。
　　人伤心时吃不下东西，狗伤心时也吃不下东西。
　　四鲜花和掌声不属于它
　　经过三个月紧张排练，《智斗大灰狼》节目终于搬上舞台。
　　高导演不愧是马戏艺术的行家里手，节目编排得热闹而有趣。帷幕拉开，两只五彩缤纷的金刚鹦鹉在道具树上互相梳理羽毛，一只可爱的小白羊迈着悠闲的步伐从树下经过，音乐优美抒情，一派和平安宁的景象。
　　大灰从草丛里钻出来，音乐骤然间变得粗野，青紫色的灯光打在它身上，模样更显得狰狞，吐着血红的狼舌，露出白森森的犬牙，发出令人毛骨悚然的长嗥。


