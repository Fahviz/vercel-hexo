---
title: Docker
categories:
  - Develop
---
# Docker安装
## 使用官方安装脚本自动安装
安装命令如下：
```sh
curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
```

也可以使用国内 daocloud 一键安装命令：
```sh
curl -sSL https://get.daocloud.io/docker | sh
```

## 卸载旧版本
较旧的 Docker 版本称为 docker 或 docker-engine 。如果已安装这些程序，请卸载它们以及相关的依赖项。

```sh
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```
# 创建并提交自己的镜像到docker hub
docker commit -a “fahviz” -m “test commit” c1abdc177915 fahviz/x-ui:v1.0

-a “lgq”为创建镜像的作者名字 -m “test commit”为提交信息 c1abdc177915 为容器id,xinaml/myjava为镜像名,v1.0为tag

# 各种部署
## A
### aria2 with rclone
```sh
docker run -d \
    --name aria2-pro \
    --restart unless-stopped \
    --log-opt max-size=1m \
    --network host \
    -e PUID=$UID \
    -e PGID=$GID \
    -e RPC_SECRET=P3TERX \
    -e RPC_PORT=6800 \
    -e LISTEN_PORT=6888 \
    -v /home/aria2/aria2-config:/config \
    -v /home/aria2/rclone-downloads:/downloads \
    -e SPECIAL_MODE=rclone \
    p3terx/aria2-pro
```
mv rclone.conf srcipt.conf to aria2-config

### ariang
```sh
docker run -d \
    --name ariang \
    --restart unless-stopped \
    --log-opt max-size=1m \
    -p 8880:6880 \
    p3terx/ariang
```

## B
### bt.cn
```sh
docker run -itd --network=host \
    -v /home/bt/:/www/wwwroot \
    --name bt --restart=unless-stopped \
    centos:7
```

## P
### Portainer
```sh
docker run -d -p 9000:9000 \
    --name portainer \
    --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v /tmp/portainer_data:/data portainer/portainer
```


## X
### X-ui
```sh
mkdir x-ui && cd x-ui
docker run -itd --network=host \
    --name x-ui --restart=unless-stopped \
    enwaiax/x-ui:latest
```
